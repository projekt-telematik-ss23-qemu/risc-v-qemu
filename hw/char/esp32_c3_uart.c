/*
 * ESP32-C3 SoC UART emulation
 *
 * Copyright (c) 2023
 * Davids Paskevics (davip00@zedat.fu-berlin.de)
 * Nicolas Patzelt (nicolas.patzelt@fu-berlin.de)
 * Jakob Knitter (jakok07@zedat.fu-berlin.de)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2 or later, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 */

#include "qemu/osdep.h"
#include "qemu/log.h"
#include "qemu/module.h"
#include "hw/char/esp32_c3_uart.h"
#include "hw/irq.h"
#include "hw/qdev-properties.h"
#include "hw/qdev-properties-system.h"
#include "migration/vmstate.h"
#include "trace.h"
#include "trace/trace-hw_char.h"

static void uart_rx_fifo_push(ESP32C3UARTState* s, uint8_t byte)
{
    s->rx_fifo[s->rx_fifo_head] = byte;
    s->rx_fifo_head++;

    if (s->rx_fifo_head >= ESP32_C3_UART_FIFO_LENGTH) {
        s->rx_fifo_head = 0;
    }

    /* Increment number of bytes in RX FIFO tracked by control register */
    uint32_t bytes = s->regs[R_UART_STATUS] & R_UART_STATUS_UART_RXFIFO_CNT_MASK;
    bytes += 1;
    s->regs[R_UART_STATUS] |= bytes;
}

static void uart_tx_fifo_push(ESP32C3UARTState* s, uint8_t byte)
{
    s->tx_fifo[s->tx_fifo_head] = byte;
    s->tx_fifo_head++;

    if (s->tx_fifo_head >= ESP32_C3_UART_FIFO_LENGTH) {
        s->tx_fifo_head = 0;
    }

    /* Increment number of bytes in TX FIFO tracked by control register */
    uint32_t bytes = s->regs[R_UART_STATUS] & R_UART_STATUS_UART_TXFIFO_CNT_MASK;
    bytes += 1;
    s->regs[R_UART_STATUS] |= bytes;
}

static bool uart_rx_fifo_pull(ESP32C3UARTState* s, uint8_t *out)
{
    if (s->rx_fifo_tail > s->rx_fifo_head) {
        trace_esp32_c3_uart_rx_fifo_underrun();
        return FALSE;
    }

    *out = s->rx_fifo[s->rx_fifo_tail];

    s->rx_fifo_tail++;

    if (s->rx_fifo_tail >= ESP32_C3_UART_FIFO_LENGTH) {
        s->rx_fifo_tail = 0;
    }

    /* Decrement number of bytes in RX FIFO tracked by control register */
    uint32_t bytes = s->regs[R_UART_STATUS] & R_UART_STATUS_UART_RXFIFO_CNT_MASK;
    bytes -= 1;
    s->regs[R_UART_STATUS] |= bytes;

    return TRUE;
}

static bool uart_tx_fifo_pull(ESP32C3UARTState* s, uint8_t *out)
{
    if (s->tx_fifo_tail > s->tx_fifo_head) {
        trace_esp32_c3_uart_tx_fifo_underrun();
        return FALSE;
    }

    *out = s->tx_fifo[s->tx_fifo_tail];

    s->tx_fifo_tail++;

    if (s->tx_fifo_tail >= ESP32_C3_UART_FIFO_LENGTH) {
        s->tx_fifo_tail = 0;
    }

    /* Decrement number of bytes in TX FIFO tracked by control register */
    uint32_t bytes = s->regs[R_UART_STATUS] & R_UART_STATUS_UART_TXFIFO_CNT_MASK;
    bytes -= 1;
    s->regs[R_UART_STATUS] |= bytes;

    return TRUE;
}

static uint64_t uart_read(void *opaque, hwaddr addr, unsigned int size)
{
    /* Note that this is for reading I/O memory, not FIFO memory */
    ESP32C3UARTState *s = ESP32_C3_UART(opaque);
    uint64_t r;
    uint8_t byte;

    switch (addr) {
    case A_UART_FIFO:
        if (!uart_rx_fifo_pull(s, &byte)) {
            r = 0;
        } else {
            r = byte;
        }
        break;
    default:
        r = s->regs[addr / 4];
        break;
    }


    trace_esp32_c3_uart_read(addr, r, size);
    return r;
}

static gboolean uart_transmit(void *do_not_use, GIOCondition cond, void *opaque)
{
    ESP32C3UARTState *s = ESP32_C3_UART(opaque);
    int r;

    if (s->tx_fifo_head == 0) {
        /* Don't have any data */
        return FALSE;
    }

    /* TODO: Bounds check / wraparound */
    uint8_t c;
    if (!uart_tx_fifo_pull(s, &c)) {
        /* FIFO underrun */
        return FALSE;
    }

    r = qemu_chr_fe_write(&s->chr, &c, 1);
    return TRUE;


    /* TODO: Raise interrupt */

    /* TODO: Figure out what this watch_tag thing is about */

    if (r <= 0) {
        /*
        s->watch_tag = qemu_chr_fe_add_watch(&s->chr, G_IO_OUT | G_IO_HUP,
                                             uart_transmit, s);
        if (!s->watch_tag) {
            goto buffer_drained;
        }
        */
        return FALSE;
    }

/*
buffer_drained:
    s->reg[R_UART_TXDRDY] = 1;
    s->pending_tx_byte = false;
    return FALSE;
    return FALSE;
*/

}

static void uart_cancel_transmit(ESP32C3UARTState *s)
{
    /* TODO: Figure out what this watch_tag business is about */
    /*
    if (s->watch_tag) {
        g_source_remove(s->watch_tag);
        s->watch_tag = 0;
    }
    */
}

static void uart_write(void *opaque, hwaddr addr,
                       uint64_t value, unsigned int size)
{
    uint8_t byte;
    ESP32C3UARTState *s = ESP32_C3_UART(opaque);

    trace_esp32_c3_uart_write(addr, value, size);

    switch (addr) {
    case A_UART_FIFO:
        /*
         * FIXME:
         * The hardware doesn't allow writing this register.
         * We only allow it here until sending is implemented properly.
        */
        byte = (value & R_UART_FIFO_UART_RXFIFO_RD_BYTE_MASK) << R_UART_FIFO_UART_RXFIFO_RD_BYTE_SHIFT;
        uart_tx_fifo_push(s, byte);
        uart_transmit(NULL, G_IO_OUT, s);
        break;
    default:
        qemu_log_mask(LOG_UNIMP, "Write to unimplemented UART control register may not have any effect\n");
        s->regs[addr / 4] = value;
        break;
    }

    /* TODO: Update IRQ (once interrupt controller is implemented) */
}

static const MemoryRegionOps uart_ops = {
    .read =  uart_read,
    .write = uart_write,
    .endianness = DEVICE_LITTLE_ENDIAN,
};

static void esp32_c3_uart_reset(DeviceState *dev)
{
    ESP32C3UARTState *s = ESP32_C3_UART(dev);

    uart_cancel_transmit(s);

    /* TODO: Fill regs w/ defaults per TRM */
    memset(s->regs, 0, sizeof(s->regs));
    memset(s->rx_fifo, 0, sizeof(s->rx_fifo));
    memset(s->tx_fifo, 0, sizeof(s->tx_fifo));

    s->rx_fifo_head = 0;
    s->rx_fifo_tail = 0;
    s->tx_fifo_head = 0;
    s->tx_fifo_tail = 0;
    s->pending_tx = false;
    s->rx_enabled = false;
    s->tx_enabled = false;
}

static void uart_receive(void *opaque, const uint8_t *buf, int size)
{
    int i;
    ESP32C3UARTState *s = ESP32_C3_UART(opaque);

    for (i = 0; i < size; i++) {
        uart_rx_fifo_push(s, buf[i]);
    }

    /* TODO: Raise interrupt */

}

static int uart_can_receive(void *opaque)
{
    ESP32C3UARTState *s = ESP32_C3_UART(opaque);

    /* Is receiving enabled? */
    if (!s->rx_enabled) {
        return 0;
    }

    return 1;
}

static void uart_event(void *opaque, QEMUChrEvent event)
{
    // ESP32C3UARTState *s = ESP32_C3_UART(opaque);

    if (event == CHR_EVENT_BREAK) {
        /* TODO: Register as error */
        /* TODO: Trigger error IRQ */
    }
}

static void esp32_c3_uart_realize(DeviceState *dev, Error **errp)
{
    ESP32C3UARTState *s = ESP32_C3_UART(dev);

    qemu_chr_fe_set_handlers(&s->chr, uart_can_receive, uart_receive,
                             uart_event, NULL, s, NULL, true);
}

static void esp32_c3_uart_init(Object *obj)
{
    ESP32C3UARTState *s = ESP32_C3_UART(obj);
    SysBusDevice *sbd = SYS_BUS_DEVICE(obj);

    memory_region_init_io(&s->iomem, obj, &uart_ops, s,
                          TYPE_ESP32_C3_UART, ESP32_C3_UART_IOMEM_SIZE);
    sysbus_init_mmio(sbd, &s->iomem);
    /* sysbus_init_irq(sbd, &s->irq); */
}

static int esp32_c3_uart_post_load(void *opaque, int version_id)
{
    /* TODO: Send pending bytes */
    /*
    ESP32C3UARTState *s = ESP32_C3_UART(opaque);

    if (s->pending_tx_byte) {
        s->watch_tag = qemu_chr_fe_add_watch(&s->chr, G_IO_OUT | G_IO_HUP,
                                             uart_transmit, s);
    }

    */

    return 0;
}

static const VMStateDescription esp32_c3_uart_vmstate = {
    .name = TYPE_ESP32_C3_UART,
    .post_load = esp32_c3_uart_post_load,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32_ARRAY(regs, ESP32C3UARTState, ESP32_C3_UART_IOMEM_SIZE_WORDS),
        VMSTATE_UINT8_ARRAY(rx_fifo, ESP32C3UARTState, ESP32_C3_UART_FIFO_LENGTH),
        VMSTATE_UINT8_ARRAY(tx_fifo, ESP32C3UARTState, ESP32_C3_UART_FIFO_LENGTH),
        VMSTATE_UINT32(rx_fifo_head, ESP32C3UARTState),
        VMSTATE_UINT32(rx_fifo_tail, ESP32C3UARTState),
        VMSTATE_UINT32(tx_fifo_head, ESP32C3UARTState),
        VMSTATE_UINT32(tx_fifo_tail, ESP32C3UARTState),
        VMSTATE_BOOL(rx_enabled, ESP32C3UARTState),
        VMSTATE_BOOL(tx_enabled, ESP32C3UARTState),
        VMSTATE_BOOL(pending_tx, ESP32C3UARTState),
        VMSTATE_END_OF_LIST()
    }
};

static Property esp32_c3_uart_properties[] = {
    DEFINE_PROP_CHR("chardev", ESP32C3UARTState, chr),
    DEFINE_PROP_END_OF_LIST(),
};

static void esp32_c3_uart_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->reset = esp32_c3_uart_reset;
    dc->realize = esp32_c3_uart_realize;
    device_class_set_props(dc, esp32_c3_uart_properties);
    set_bit(DEVICE_CATEGORY_INPUT, dc->categories);
    dc->vmsd = &esp32_c3_uart_vmstate;
}

static const TypeInfo esp32_c3_uart_info = {
    .name = TYPE_ESP32_C3_UART,
    .parent = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(ESP32C3UARTState),
    .instance_init = esp32_c3_uart_init,
    .class_init = esp32_c3_uart_class_init
};

static void esp32_c3_uart_register_types(void)
{
    type_register_static(&esp32_c3_uart_info);
}

type_init(esp32_c3_uart_register_types)
