/*
 * ESP32-C3-class SoC emulation
 *
 * Copyright (c) 2023
 * Davids Paskevics (davip00@zedat.fu-berlin.de)
 * Nicolas Patzelt (nicolas.patzelt@fu-berlin.de)
 * Jakob Knitter (jakok07@zedat.fu-berlin.de)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2 or later, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 */

#include "qemu/osdep.h"

#include "hw/char/esp32_c3_uart.h"
#include "qemu/log.h"
#include "hw/boards.h"
#include "hw/riscv/esp32_c3.h"
#include "qapi/error.h"
#include "qemu/error-report.h"
#include "hw/intc/riscv_aclint.h"
#include "sysemu/sysemu.h"
#include "hw/qdev-properties.h"
#include "exec/address-spaces.h"
#include "hw/riscv/boot.h"
#include "hw/misc/unimp.h"
#include "hw/qdev-core.h"


static const struct MemmapEntry {
    hwaddr base;
    hwaddr size;
} esp32_c3_memmap[] = {
    [ESP32_C3_DUMMY]  =  {  0x3C800000,  0x0000F   },
    [ESP32_C3_DROM]   =  {  0x3FF00000,  0x1FFFF   },
    [ESP32_C3_IROM]   =  {  0x40000000,  0x5FFFF   },
    [ESP32_C3_IRAM]   =  {  0x4037C000,  0x63FFF   },
    [ESP32_C3_DRAM]   =  {  0x3FC80000,  0x5FFFF   },
    [ESP32_C3_DM]     =  {  0x20000000,  0x7FFFFFF }
};

/* Reset vector according to technical manual page 217 */
// static const hwaddr esp32_c3_entry = 0x40000400;
/* System I/O memory (TRM p. 91) */
static const hwaddr esp32_c3_sysregs = 0x600C0000;
static const hwaddr esp32_c3_sysregs_size = 4096;
/* Clock control/configuration register */
static const hwaddr esp32_c3_clkcfg = esp32_c3_sysregs + 0x0000;

// Stub implementation for the clock peripheral
static uint64_t clock_read(void *opaque, hwaddr addr, unsigned int size)
{
    qemu_log_mask(LOG_UNIMP, "%s: 0x%" HWADDR_PRIx " [%u]\n",
                  __func__, addr, size);
    return 1;
}

static void clock_write(void *opaque, hwaddr addr, uint64_t data,
                        unsigned int size)
{
    qemu_log_mask(LOG_UNIMP, "%s: 0x%" HWADDR_PRIx " <- 0x%" PRIx64 " [%u]\n",
                  __func__, addr, data, size);
}

static const MemoryRegionOps clock_ops = {
    .read = clock_read,
    .write = clock_write
};

static void esp32_c3_setup_reset_vec(void) {
    // TODO: Implement
}

static void esp32_c3_machine_state_init(MachineState *mstate)
{
    Esp32C3MachineState *sms = RISCV_ESP32_MACHINE(mstate);
    MemoryRegion *system_memory = get_system_memory();

    /* Allow only ESP32 C3 CPU for this platform */
    if (strcmp(mstate->cpu_type, TYPE_RISCV_CPU_ESP32_C3) != 0) {
        error_report("This board can only be used with ESP32 C3 CPU");
        exit(1);
    }

    /* Initialize SoC */
    object_initialize_child(OBJECT(mstate), "soc", &sms->soc,
                            TYPE_RISCV_ESP32_SOC);
    qdev_realize(DEVICE(&sms->soc), NULL, &error_abort);

    /* register RAM */
    // FIXME: Do we really need to do this? The device is realized right afterwards
    memory_region_add_subregion(system_memory,
                                esp32_c3_memmap[ESP32_C3_IRAM].base,
                                mstate->ram);

    esp32_c3_setup_reset_vec();

    if (!mstate->firmware) {
        // Firmware is required for most programs, as they call directly into it
        error_report("This board requires a firmware image to run");
        exit(1);
    }

    if (mstate->kernel_filename) {
        // Can't load a Linux-style kernel on an MCU
        error_report("-device loader,file=<filename> must be used instead of -kernel");
        exit(1);
    }

    if (mstate->fdt) {
        // Can't load a device tree on an MCU
        error_report("-dtb is not supported on this board");
        exit(1);
    }

    if (mstate->initrd_filename) {
        // Can't load an initrd on an MCU
        error_report("-initrd is not supported on this board");
        exit(1);
    }
}

static void esp32_c3_machine_instance_init(Object *obj)
{
}

static void esp32_c3_machine_class_init(ObjectClass *klass, void *data)
{
    MachineClass *mc = MACHINE_CLASS(klass);
    mc->desc = "RISC-V Board compatible with ES32-C3 SDK";
    mc->init = esp32_c3_machine_state_init;
    mc->default_cpu_type = TYPE_RISCV_CPU_ESP32_C3;
    // We have to pass something here, but want to initialize the RAM ourselves
    mc->default_ram_id = "riscv.esp32.c.dummyram";
}

static const TypeInfo esp32_c3_machine_type_info = {
    .name = TYPE_RISCV_ESP32_MACHINE,
    .parent = TYPE_MACHINE,
    .class_init = esp32_c3_machine_class_init,
    .instance_init = esp32_c3_machine_instance_init,
    .instance_size = sizeof(Esp32C3MachineState),
};

static void esp32_c3_machine_type_info_register(void)
{
    type_register_static(&esp32_c3_machine_type_info);
}
type_init(esp32_c3_machine_type_info_register)

static void esp32_c3_soc_state_realize(DeviceState *dev, Error **errp)
{
    MemoryRegion* uart_mem;

    MachineState *ms = MACHINE(qdev_get_machine());
    (void)ms;
    Esp32C3SoCState *sss = RISCV_ESP32_SOC(dev);
    MemoryRegion *sys_mem = get_system_memory();

    sysbus_realize(SYS_BUS_DEVICE(&sss->cpus), &error_abort);

    /* register ROM */
    memory_region_init_rom(&sss->irom, OBJECT(dev), "riscv.esp32.c.irom",
                           esp32_c3_memmap[ESP32_C3_IROM].size, &error_fatal);
    memory_region_add_subregion(sys_mem, esp32_c3_memmap[ESP32_C3_IROM].base,
                                &sss->irom);
    memory_region_init_rom(&sss->drom, OBJECT(dev), "riscv.esp32.c.drom",
                           esp32_c3_memmap[ESP32_C3_DROM].size, &error_fatal);
    memory_region_add_subregion(sys_mem, esp32_c3_memmap[ESP32_C3_DROM].base,
                                &sss->drom);

    /* register RAM */
    memory_region_init_ram(&sss->iram, OBJECT(dev), "riscv.esp32.c.iram",
                           esp32_c3_memmap[ESP32_C3_IRAM].size, &error_fatal);
    memory_region_add_subregion(sys_mem, esp32_c3_memmap[ESP32_C3_IRAM].base,
                                &sss->iram);
    memory_region_init_ram(&sss->dram, OBJECT(dev), "riscv.esp32.c.dram",
                           esp32_c3_memmap[ESP32_C3_DRAM].size, &error_fatal);
    memory_region_add_subregion(sys_mem, esp32_c3_memmap[ESP32_C3_DRAM].base,
                                &sss->dram);

    // TODO: Do we need to register dummy RAM here?

    /* register dummy peripherals */
    /*
    Top-level instance owning all I/O memory.
    Not sure if we need this, but NRF51/microbit does it this way.
    */
    memory_region_init(&sss->container,NULL, "riscv.esp32.c.container", UINT64_MAX);
    memory_region_init_io(&sss->clock, NULL, &clock_ops, NULL,
                          "riscv.esp32.c.clock", 0x0058);
    memory_region_add_subregion_overlap(&sss->container,
                                        esp32_c3_clkcfg, &sss->clock, -1);
    create_unimplemented_device("riscv.esp32.c.sysregs", esp32_c3_sysregs, esp32_c3_sysregs_size);

    /* Register implemented peripherals */
    qdev_prop_set_chr(DEVICE(&(sss->uart)), "chardev", serial_hd(0));
    if (!sysbus_realize(SYS_BUS_DEVICE(&sss->uart), errp)) {
        return;
    }
    uart_mem = sysbus_mmio_get_region(SYS_BUS_DEVICE(&sss->uart), 0);
    memory_region_add_subregion_overlap(&sss->container, ESP32_C3_UART0_BASE, uart_mem, 0);

    /* Load boot ROM dump into emulated ROM */
    riscv_load_firmware(ms->firmware, esp32_c3_memmap[ESP32_C3_IROM].base, NULL);
}

static void esp32_c3_soc_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);
    dc->realize = esp32_c3_soc_state_realize;
    /*
     * Reasons:
     *     - Creates CPUS in riscv_hart_realize(), and can create unintended
     *       CPUs
     *     - Uses serial_hds in realize function, thus can't be used twice
     */
    dc->user_creatable = false;
}

static void esp32_c3_soc_instance_init(Object *obj)
{
    Esp32C3SoCState *sss = RISCV_ESP32_SOC(obj);

    object_initialize_child(obj, "cpus", &sss->cpus, TYPE_RISCV_HART_ARRAY);

    /*
     * CPU type is fixed and we are not supporting passing from commandline yet.
     * So let it be in instance_init. When supported should use ms->cpu_type
     * instead of TYPE_RISCV_CPU_ESP32_C3
     */
    object_property_set_str(OBJECT(&sss->cpus), "cpu-type",
                            TYPE_RISCV_CPU_ESP32_C3, &error_abort);
    object_property_set_int(OBJECT(&sss->cpus), "num-harts", 1,
                            &error_abort);

    /* Init peripherals */
    object_initialize_child(obj, "uart", &sss->uart, TYPE_ESP32_C3_UART);
    //object_property_add_alias(obj, "serial0", &sss->uart, "chardev");
}

static const TypeInfo esp32_c3_type_info = {
    .name = TYPE_RISCV_ESP32_SOC,
    .parent = TYPE_DEVICE,
    .class_init = esp32_c3_soc_class_init,
    .instance_init = esp32_c3_soc_instance_init,
    .instance_size = sizeof(Esp32C3SoCState),
};

static void esp32_c3_type_info_register(void)
{
    type_register_static(&esp32_c3_type_info);
}

type_init(esp32_c3_type_info_register)
