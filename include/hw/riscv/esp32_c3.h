/*
 * ESP32-C3-class SoC emulation
 *
 * Copyright (c) 2023
 * Davids Paskevics (davip00@zedat.fu-berlin.de)
 * Nicolas Patzelt (nicolas.patzelt@fu-berlin.de)
 * Jakob Knitter (jakok07@zedat.fu-berlin.de)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2 or later, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HW_ESP32_C3_H
#define HW_ESP32_C3_H

#include "hw/riscv/riscv_hart.h"
#include "hw/boards.h"
#include "qemu/typedefs.h"
#include "hw/char/esp32_c3_uart.h"



#define TYPE_RISCV_ESP32_SOC "riscv.esp32.cclass.soc"
#define RISCV_ESP32_SOC(obj) \
    OBJECT_CHECK(Esp32C3SoCState, (obj), TYPE_RISCV_ESP32_SOC)

typedef struct Esp32C3SoCState {
    /*< private >*/
    DeviceState parent_obj;

    /*< public >*/
    RISCVHartArrayState cpus;
    DeviceState *plic;
    /* RAM / ROM */
    MemoryRegion dummy;
    MemoryRegion irom;
    MemoryRegion drom;
    MemoryRegion iram;
    MemoryRegion dram;
    /* Memory mapped I/O devices */
    MemoryRegion container;
    MemoryRegion clock;
    /* Misc. devices */
    ESP32C3UARTState uart;

} Esp32C3SoCState;

#define TYPE_RISCV_ESP32_MACHINE MACHINE_TYPE_NAME("esp32_c3")
#define RISCV_ESP32_MACHINE(obj) \
    OBJECT_CHECK(Esp32C3MachineState, (obj), TYPE_RISCV_ESP32_MACHINE)
typedef struct Esp32C3MachineState {
    /*< private >*/
    MachineState parent_obj;

    /*< public >*/
    Esp32C3SoCState soc;
} Esp32C3MachineState;

enum {
    ESP32_C3_DUMMY,
    ESP32_C3_DROM,
    ESP32_C3_IROM,
    ESP32_C3_IRAM,
    ESP32_C3_DRAM,
    ESP32_C3_DM
};

#endif
