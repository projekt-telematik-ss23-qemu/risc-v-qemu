/*
 * ESP32-C3 SoC UART emulation
 *
 * Copyright (c) 2023
 * Davids Paskevics (davip00@zedat.fu-berlin.de)
 * Nicolas Patzelt (nicolas.patzelt@fu-berlin.de)
 * Jakob Knitter (jakok07@zedat.fu-berlin.de)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2 or later, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 */

#ifndef ESP32_C3_UART_H
#define ESP32_C3_UART_H

#include "hw/sysbus.h"
#include "chardev/char-fe.h"
#include "hw/registerfields.h"
#include "qom/object.h"


#define TYPE_ESP32_C3_UART "esp32c3-uart"
#define ESP32_C3_UART(obj) \
    OBJECT_CHECK(ESP32C3UARTState, (obj), TYPE_ESP32_C3_UART)

#define ESP32_C3_UART0_BASE 0x60000000
#define ESP32_C3_UART1_BASE 0x60001000
#define ESP32_C3_UART_IOMEM_SIZE 0x1000 /* 4K */
#define ESP32_C3_UART_IOMEM_SIZE_WORDS (ESP32_C3_UART_IOMEM_SIZE / 4)

#define ESP32_C3_UART_FIFO_LENGTH 128

/* These are all relative to the base address */

REG32(UART_FIFO, 0x0000) /* FIFO read via this register */
    FIELD(UART_FIFO, UART_RXFIFO_RD_BYTE, 0, 8);
REG32(UART_CLKDIV, 0x0014)
REG32(UART_RX_FILT, 0x0018)
REG32(UART_STATUS, 0x001C)
    FIELD(UART_STATUS, UART_RXFIFO_CNT, 0, 10)
    FIELD(UART_STATUS, UART_TXFIFO_CNT, 16, 10)
REG32(UART_CONF0, 0x0020)
    FIELD(UART_CONF0, PARITY, 0, 1)         /* Parity config */
    FIELD(UART_CONF0, PARITY_EN, 1, 1)      /* Parity enable */
    FIELD(UART_CONF0, BIT_NUM, 2, 1)        /* Number of bits per transmission */
    FIELD(UART_CONF0, STOP_BIT_NUM, 4, 1)   /* Number of stop bits per transmission */
REG32(UART_CLK_CONF, 0x0078)
REG32(UART_ID, 0x0080)

typedef struct {
    SysBusDevice parent_obj;
    CharBackend chr; /* Tell qemu this is a character device */

    MemoryRegion iomem; /* Config registers memory region */
    uint32_t regs[ESP32_C3_UART_IOMEM_SIZE_WORDS]; /* Actual contents of config registers */

    MemoryRegion fifo;  /* UART FIFO RAM memory region */
    uint8_t rx_fifo[ESP32_C3_UART_FIFO_LENGTH]; /* RX FIFO RAM */
    uint8_t tx_fifo[ESP32_C3_UART_FIFO_LENGTH]; /* TX FIFO RAM */

    unsigned int rx_fifo_head; /* Head of RX FIFO (bytes are appended here) */
    unsigned int rx_fifo_tail; /* Tail of RX FIFO (bytes are read from here) */
    unsigned int tx_fifo_head; /* Head of TX FIFO (bytes are appended here) */
    unsigned int tx_fifo_tail; /* Tail of TX FIFO (bytes are read from here) */
    bool rx_enabled;
    bool tx_enabled;
    bool pending_tx;
} ESP32C3UARTState;

#endif
